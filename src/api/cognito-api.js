import {CognitoAuth} from 'amazon-cognito-auth-js';
import {config} from '../config';

const authData = {
  ClientId: '1kde6pa1lrafo2s47giooq5hrg',
  AppWebDomain: 'seiso-home-pool.auth.eu-west-1.amazoncognito.com',
  TokenScopesArray: ['email', 'openid'],
  RedirectUriSignIn: config.RedirectUriSignIn,
  RedirectUriSignOut: config.RedirectUriSignOut,
};
const auth = new CognitoAuth(authData);
auth.useCodeGrantFlow();

const getJwtToken = () => {
  const session = auth.getSession(); return session ? session.idToken.jwtToken : undefined;
};
const isUserSignedIn = () => auth.isUserSignedIn();
const getAuth = () => auth;
const signOut = () => auth.signOut();
const getEmail = () => {
  if (isUserSignedIn()) {
    const session = auth.getSession(); return session ? session.idToken.payload.email : undefined;
  } else {
    return undefined;
  }
};
auth.userhandler = {
  onSuccess: function(result) {
    console.log('onSuccess', result);
    return result;
  },
  onFailure: function(err) {
    console.log('onFailure', err);
    return err;
  }
};

export default {
  getJwtToken,
  isUserSignedIn,
  getAuth,
  signOut,
  getEmail
};
