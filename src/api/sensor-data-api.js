import axios from 'axios';
import {config} from '../config';

const getLatestSensorValue = async (sensorId) => {
  const response = await axios.get(config.sensorApiBaseUrl + `/sensor-data/${sensorId}/latest`);
  return response.data;
};

export default {
  getLatestSensorValue
};
