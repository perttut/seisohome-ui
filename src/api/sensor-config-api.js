import axios from 'axios';
import {config} from '../config';

const getSensorConfig = async () => {
  const response = await axios.get(config.sensorApiBaseUrl + '/sensor-config');
  return response.data;
};

const postSensorConfig = async (sensorConfig) => {
  await axios.post(config.sensorApiBaseUrl + '/sensor-config', [sensorConfig]);
};

const deleteSensorConfig = async (sensorConfigId) => {
  return await axios.delete(config.sensorApiBaseUrl + `/sensor-config/${sensorConfigId}`);
};

export default {
  getSensorConfig,
  postSensorConfig,
  deleteSensorConfig
};
