import axios from 'axios';
import React from 'react';
import {Router} from '@reach/router';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

import Code from './Login';
import NavBar from './components/NavBar';
import User from './components/User';
import Settings from './components/Settings';
import {config} from './config';
import Temperature from './components/Temperature';
import reducers from './reducers';
import cognitoApi from './api/cognito-api';

const setAxiosAuthorizationInterceptor = (cognitoApi) => {
  axios.interceptors.request.use(
    axiosConfig => {
      // send token to only to our API
      if (axiosConfig.url.startsWith(config.sensorApiBaseUrl) && !axiosConfig.headers.Authorization) {
        // TODO: handle missing JWTToken (expiration case)
        axiosConfig.headers.Authorization = cognitoApi.getJwtToken();
      }

      return axiosConfig;
    },
    error => Promise.reject(error)
  );
};

const Home = () => {
  return (
    <h1>Home</h1>
  );
};

const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
  ));

const App = () => {

  setAxiosAuthorizationInterceptor(cognitoApi);
  return (
    <div>
      <Provider store={store}>
        <NavBar/>
        <Router>
          <Code path="code"/>
          <Home path="/"/>
          <Settings path="settings"/>
          <User path="user"/>
          <Temperature path="temperature"/>
        </Router>
      </Provider>
    </div>
  );
};

export default App;
