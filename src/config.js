import configLocal from './config-local';

export const config = {
  sensorApiBaseUrl: 'https://nzfyvixjm2.execute-api.eu-west-1.amazonaws.com/production',
  RedirectUriHome: 'https://d31brfs8zm59md.cloudfront.net',
  RedirectUriSignIn: 'https://d31brfs8zm59md.cloudfront.net/code',
  RedirectUriSignOut: 'https://d31brfs8zm59md.cloudfront.net/logout',
  ...configLocal() // TODO: Cheap workaround for to override configuration during local development
};
