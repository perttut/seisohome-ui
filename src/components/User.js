import React from 'react';
import {connect} from 'react-redux';
import {login, logout, clearTokens} from '../actions';
import PropTypes from 'prop-types';

const User = (props) => {
  let content;
  if (props.user) {
    content = (
      <div>
        <button onClick={() => props.logout()}>Logout</button>
        <button onClick={() => props.clearTokens()}>Clear tokens</button>
      </div>
    );
  } else {
    content = (
      <div>
        <button onClick={() => props.login()}>Login</button>;
        <button onClick={() => props.clearTokens()}>Clear tokens</button>
      </div>
    );
  }

  return content;
};

const mapStateToProps = (state) => {
  return {user: state.user};
};

User.propTypes = {
  user: PropTypes.object.isRequired
};

export default connect(mapStateToProps, ({login, logout, clearTokens}))(User);
