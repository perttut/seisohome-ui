import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {SettingsEditModal} from './SettingsEditModal';
import {deleteSensorConfig, loadSensorConfig, updateSensorConfig} from '../actions';

const SensorRow = (props) => {
  return (
    <tr>
      <td>{props.sid}</td>
      <td>{props.name}</td>
      <td>{props.hardwareId}</td>
      <td>{props.unit}</td>
      <td>{props.enabled ? 'true' : 'false'}</td>
      <td><button onClick={props.edit}>Edit</button></td>
      <td><button onClick={props.delete}>Delete</button></td>
    </tr>
  );
};

SensorRow.propTypes = {
  sid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  hardwareId: PropTypes.string.isRequired,
  unit: PropTypes.string.isRequired,
  enabled: PropTypes.bool.isRequired,
  edit: PropTypes.func.isRequired,
  delete: PropTypes.func.isRequired
};

const Settings = (props) => {

  const [showModal, setShowModal] = useState(false);
  const [createNewConfig, setCreateNewConfig] = useState(false);
  const [editedConfig, setEditedConfig] = useState({});

  const {loadSensorConfig} = props; // get rid of eslint react-hooks/exhaustive-deps complaint
  useEffect(() => {
    loadSensorConfig();
  }, [loadSensorConfig]);

  const sensorRows = props.sensorConfig.map(sc => (
    <SensorRow
      key={sc.id}
      name={sc.name}
      sid={sc.id}
      hardwareId={sc.hardwareId}
      unit={sc.unit}
      enabled={sc.enabled}
      edit={() => { setShowModal(true); setCreateNewConfig(false); setEditedConfig(sc); }}
      delete={() => props.deleteSensorConfig(sc.id)}
    />)
  );

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>id</th>
            <th>name</th>
            <th>hardware id</th>
            <th>unit</th>
            <th>enabled</th>
            <th>edit</th>
            <th>delete</th>
          </tr>
        </thead>
        <tbody>
          {sensorRows}
        </tbody>
      </table>
      <SettingsEditModal
        sc={editedConfig}
        show={showModal}
        setShow={setShowModal}
        createNew={createNewConfig}
        updateSensorConfig={props.updateSensorConfig}/>
      <button onClick={
        () => { setShowModal(true); setCreateNewConfig(true); setEditedConfig({}); }}>
        Create
      </button>
    </div>
  );
};

Settings.propTypes = {
  sensorConfig: PropTypes.array,
  loadSensorConfig: PropTypes.func,
  updateSensorConfig: PropTypes.func,
  deleteSensorConfig: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    sensorConfig: state.sensorConfig
  };
};

export default connect(mapStateToProps, {loadSensorConfig, updateSensorConfig, deleteSensorConfig})(Settings);
