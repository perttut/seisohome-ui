import React from 'react';
import PropTypes from 'prop-types';
import './Card.css';

export const Card = (props) => {
  return (
    <div className="card">
      <div className="card-header">{props.header}</div>
      <div className="card-content">{props.children}</div>
    </div>
  );
};

Card.propTypes = {
  header: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
};
