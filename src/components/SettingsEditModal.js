import {Field, Form} from 'react-final-form';
import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from './Modal';
import './SettingsEditModal.css';

const TextField = (props) => (
  <Field
    id={props.id}
    name={props.name}
    component="input"
    type="text"
    disabled={props.disabled ? 'disabled' : ''}
  />
);

TextField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

const CheckBoxField = (props) => (
  <Field
    id={props.id}
    name={props.name}
    component="input"
    type="checkbox"
  />
);

CheckBoxField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

const SensorConfig = (props) => {
  return (
    <div className={'sensor-config'}>
      <label htmlFor="sid">id</label>
      <TextField id="sid" name={'id'} disabled={!props.createNew}/>
      <label htmlFor="name">name</label>
      <TextField id="name" name={'name'} />
      <label htmlFor="hardwareId">hardware id</label>
      <TextField id="hardwareId" name={'hardwareId'}/>
      <label htmlFor="unit">unit</label>
      <TextField id="unit" name={'unit'}/>
      <label htmlFor="enabled">enabled</label>
      <CheckBoxField id="enabled" name={'enabled'}/>
    </div>
  );
};

SensorConfig.propTypes = {
  sc: PropTypes.object.isRequired,
  createNew: PropTypes.bool
};

export const SettingsEditModal = (props) => {
  const onSubmit = async (sensorConfig) => {
    await props.updateSensorConfig(sensorConfig);
    props.setShow(false);
  };

  const initialValues = {
    id: props.sc.id,
    name: props.sc.name,
    hardwareId: props.sc.hardwareId,
    unit: props.sc.unit,
    enabled: props.sc.enabled ? props.sc.enabled : false // in case sc is undefined, default to false
  };

  return (
    <Modal
      show={props.show}
      handleClose={() => props.setShow(false)}>

      <Form
        initialValues={initialValues}
        onSubmit={onSubmit}
        render={({handleSubmit, form, submitting, pristine, values}) => (
          <form onSubmit={handleSubmit}>
            <SensorConfig sc={props.sc} createNew={props.createNew}/>
            <button type="submit" disabled={submitting || pristine}>
              Submit
            </button>
          </form>
        )}/>

    </Modal>
  );
};

SettingsEditModal.propTypes = {
  sc: PropTypes.object.isRequired,
  show: PropTypes.bool,
  setShow: PropTypes.func.isRequired,
  createNew: PropTypes.bool,
  updateSensorConfig: PropTypes.func.isRequired
};
