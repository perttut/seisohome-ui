import {Link} from '@reach/router';
import React from 'react';
import {connect} from 'react-redux';
import './NavBar.css';
import PropTypes from 'prop-types';

const NavBar = (props) => {
  return (
    <nav>
      <div className="nav-bar-content">
        <Link to="/">Home</Link>
        <Link to="temperature">Temperature</Link>
      </div>
      <div className="nav-bar-content nav-bar-content-right">
        <Link to="settings">Settings</Link>
        <Link to="user">{props.user ? props.user.email: 'Login'}</Link>
      </div>
    </nav>
  );
};

NavBar.propTypes = {
  user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {user: state.user};
};

export default connect(mapStateToProps)(NavBar);
