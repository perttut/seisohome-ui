import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {useInterval} from '../hooks/useInterval';
import {connect} from 'react-redux';
import {loadSensorDataAndSensorConfig} from '../actions';
import {Card} from './Card';
import './Temperature.css';

// TODO: replace hard codings with dynamic configuration
const hardCodedSensors = new Set(['outsideFrontTemp', 'fridgeTemp', 'freezerTemp']);

const Temperature = (props) => {
  const [sensors, setSensors] = useState([]);

  useInterval(() => {
    props.loadSensorDataAndSensorConfig(Array.from(hardCodedSensors));
  }, 10*1000);

  const updateSensorValues = async (sensors, idToSensorData, setSensors) => {
    const updateSensorValue = (sensor, idToSensorData) => {
      if (idToSensorData.has(sensor.id)) {
        const scValue = idToSensorData.get(sensor.id);
        return {
          ...sensor,
          value: scValue.value,
          timestamp: scValue.timestamp
        };
      } else {
        return sensor;
      }
    };
    const updatedSensorValues = sensors.map(s => updateSensorValue(s, idToSensorData));
    setSensors(updatedSensorValues);
  };

  const {loadSensorDataAndSensorConfig} = props; // get rid of eslint react-hooks/exhaustive-deps complaint
  useEffect(() => {
    loadSensorDataAndSensorConfig(Array.from(hardCodedSensors));
  }, [loadSensorDataAndSensorConfig]);

  useEffect(() => {
    const scsInUse = props.sensorConfig.filter(sc => hardCodedSensors.has(sc.id));
    const scsWithValues = scsInUse.map(sc => ({
      ...sc,
      value: undefined,
      timestamp: undefined
    }));
    updateSensorValues(scsWithValues, props.sensorData, setSensors);
  }, [props.sensorConfig, props.sensorData]);

  const cardContent = (temperature, timestamp) => {
    return (
      <div className="temperature-card-content">
        <span>{temperature ? temperature.toFixed(2) : '--.--'} &#8451;</span>
        <span>({timestamp ? new Date(timestamp).toLocaleTimeString('fi-FI', { hour12: false }) : '----'})</span>
      </div>
    );
  };

  const render = sensors.map(s => <Card key={s.id} header={s.name}>{cardContent(s.value, s.timestamp)}</Card>);
  return (<div className="temperature">{render}</div>);
};

Temperature.propTypes = {
  sensorConfig: PropTypes.array,
  sensorData: PropTypes.object,
  loadSensorDataAndSensorConfig: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    sensorConfig: state.sensorConfig,
    sensorData: state.sensorData
  };
};

export default connect(mapStateToProps, {loadSensorDataAndSensorConfig})(Temperature);
