import React from 'react';
import PropTypes from 'prop-types';
import './Modal.css';

export const Modal = (props) => {
  const showHideClassName = props.show ? 'modal display-block' : 'modal display-none';
  const children = props.show ? props.children : <div></div>;

  return (
    <div className={showHideClassName}>
      <div className='modal-main'>
        {children}
        <button onClick={props.handleClose}>
          Close
        </button>
      </div>
    </div>
  );
};

Modal.propTypes = {
  show: PropTypes.bool.isRequired,
  children: PropTypes.element,
  handleClose: PropTypes.func.isRequired,
};
