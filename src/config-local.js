export default function configLocal() {
  if (process.env.NODE_ENV === 'development') {
    console.log('RUNNING IN DEVELOPMENT MODE');
    return {
      RedirectUriHome: 'http://localhost:3000',
      RedirectUriSignIn: 'http://localhost:3000/code',
      RedirectUriSignOut: 'http://localhost:3000/logout'
    };
  }
  return {};
}
