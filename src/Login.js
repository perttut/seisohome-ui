import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Location} from '@reach/router';
import {connect} from 'react-redux';
import {oauth2Code} from './actions';

const Code = (props) => {
  return (
    <Location>
      {
        ({location}) => {
          return (<LoginCode codeAction={props.oauth2Code} location={location}/>);
        }
      }
    </Location>
  );
};

Code.propTypes = {
  oauth2Code: PropTypes.func
};

const LoginCode = (props) => {
  useEffect(() => {
    props.codeAction(props.location.search);
  });
  return <b>Redirecting...</b>;
};

LoginCode.propTypes = {
  location: PropTypes.object.isRequired,
  codeAction: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return {user: state.user};
};

export default connect(mapStateToProps, ({oauth2Code}))(Code);
