import {combineReducers} from 'redux';
import cognitoApi from '../api/cognito-api';
import {
  SENSOR_CONFIG_DELETE,
  SENSOR_CONFIG_LOAD,
  SENSOR_CONFIG_UPDATE,
  SENSOR_DATA_LOAD
} from '../actions/action-types';

/***********************
 * User
 ***********************/

const initialUser = () => {
  if (cognitoApi.isUserSignedIn()) {
    return {
      email: cognitoApi.getEmail()
    };
  }
  return null;
};

export const userReducer = (user = initialUser(), action) => {
  switch (action.type) {
    default:
      return user;
  }
};

/***********************
 * Sensor config
 ***********************/

const updateSensorConfig = (sensorConfig, singleSensorConfig) => {
  const editedIndex = sensorConfig.findIndex(sc => sc.id === singleSensorConfig.id);
  let updatedSensorConfig;
  if (editedIndex === -1) {
    updatedSensorConfig = [...sensorConfig, singleSensorConfig];
  } else {
    updatedSensorConfig = [...sensorConfig];
    updatedSensorConfig[editedIndex] = singleSensorConfig;
  }
  return updatedSensorConfig;
};

export const sensorConfigReducer = (sensorConfig = [], action) => {
  switch (action.type) {
    case SENSOR_CONFIG_LOAD:
      return action.payload;
    case SENSOR_CONFIG_UPDATE:
      console.log('SENSOR_CONFIG_UPDATE', sensorConfig, action);
      return updateSensorConfig(sensorConfig, action.payload);
    case SENSOR_CONFIG_DELETE:
      return sensorConfig.filter(sc => sc.id !== action.payload);
    default:
      return sensorConfig;
  }
};

/***********************
 * Sensor data
 ***********************/

export const sensorDataReducer = (sensorData = new Map(), action) => {
  switch (action.type) {
    case SENSOR_DATA_LOAD:
      return action.payload;
    default:
      return sensorData;
  }
};



export default combineReducers({
  user: userReducer,
  sensorConfig: sensorConfigReducer,
  sensorData: sensorDataReducer
});
