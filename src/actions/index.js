import {config} from '../config';
import cognitoApi from '../api/cognito-api';
import sensorConfigApi from '../api/sensor-config-api';
import {
  SENSOR_CONFIG_DELETE,
  SENSOR_CONFIG_LOAD,
  SENSOR_CONFIG_UPDATE,
  SENSOR_DATA_LOAD
} from './action-types';
import sensorDataApi from '../api/sensor-data-api';

export const login = () => async (dispatch) => {
  cognitoApi.getAuth().getSession();
};

export const logout = () => async (dispatch) => {
  cognitoApi.signOut();
};

// if token goes old, UI should do something smart like force user to login again. But before UI is smart enough, it's
// enough to let user clear tokens manually and do login again...
export const clearTokens = () => async (dispatch) => {
  const keys = Object.keys(window.localStorage);
  const cognitoKeys = keys.filter(key => key.startsWith('CognitoIdentityServiceProvider'));
  cognitoKeys.forEach(key => window.localStorage.removeItem(key));
  window.location.href = config.RedirectUriHome;
};


export const oauth2Code = (urlParameters) => async (dispatch, getState)  => {
  if (!cognitoApi.isUserSignedIn()) {
    // A hack for getting redirect to root working only after this component has handled parseCognitoWebResponse()
    let resolveOut;
    let rejectOut;
    const promFunc = (resolve, reject) => {
      resolveOut = resolve;
      rejectOut = reject;
    };
    const onResultPromise = new Promise(promFunc);
    const onSuccess = (result) => resolveOut(result);
    const onFailure = (failure) => rejectOut(failure);
    cognitoApi.getAuth().userhandler = {onSuccess, onFailure};
    cognitoApi.getAuth().parseCognitoWebResponse(urlParameters);
    try {
      await onResultPromise;
      // redirect to application home to re-initialize app with the access tokens in local storage (this avoids
      // having /code as the refresh url of the application)
      window.location.href = config.RedirectUriHome;
    } catch (e) {
      console.error(e);
      alert('Login failed!');
      // TODO: add redirect to somewhere later...
    }
  }
};

export const loadSensorConfig = () => async dispatch => {
  const sensorConfig = await sensorConfigApi.getSensorConfig();
  dispatch(sensorConfigLoad(sensorConfig));
};

const sensorConfigLoad = (sensorConfig) => {
  return {
    type: SENSOR_CONFIG_LOAD,
    payload: sensorConfig
  };
};

export const updateSensorConfig = (singleSensorConfig) => async (dispatch) => {
  await sensorConfigApi.postSensorConfig(singleSensorConfig);
  dispatch(sensorConfigUpdate(singleSensorConfig));
};

const sensorConfigUpdate = (singleSensorConfig) => {
  return {
    type: SENSOR_CONFIG_UPDATE,
    payload: singleSensorConfig
  };
};

export const deleteSensorConfig = (sensorConfigId) => async (dispatch, getState) => {
  await sensorConfigApi.deleteSensorConfig(sensorConfigId);
  dispatch(sensorConfigDelete(sensorConfigId));
};

const sensorConfigDelete = (sensorConfigId) => {
  return {
    type: SENSOR_CONFIG_DELETE,
    payload: sensorConfigId
  };
};


export const loadSensorDataAndSensorConfig = (sensorIds) => async (dispatch, getState) => {
  const loadDataPromises = sensorIds.map(sid => sensorDataApi.getLatestSensorValue(sid));
  if (getState().sensorConfig.length === 0) {
    // load only if not preloaded before
    await dispatch(loadSensorConfig());
  }
  const sensorValues = await Promise.all(loadDataPromises);
  const idToSensorData = new Map(sensorValues.reduce((acc, curr) => [...acc, [curr.id, curr]], []));
  dispatch(sensorDataLoad(idToSensorData));
};

const sensorDataLoad = (sensorData) => {
  return {
    type: SENSOR_DATA_LOAD,
    payload: sensorData
  };
};
